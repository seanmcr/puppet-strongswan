# strongSwan charon::farp class.
class strongswan::charon::farp (
  $farp_enabled          = 'no',
) inherits strongswan::params {

  file { 'farp.conf':
    ensure  => present,
    path    => $strongswan::params::farp_conf,
    content => template("${module_name}/charon_farp.conf.erb"),
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
    require => Package[$strongswan::params::package],
    notify  => Class['Strongswan::Service'],
  }
}