require 'spec_helper'

describe 'strongswan::charon::farp', :type => 'class' do
  context "on a Debian OS" do
    let :facts do
      {
        :osfamily       => 'Debian',
      }
    end

    it {
      should contain_file('farp.conf').with(
        "ensure"  => "present",
        "path"    => "/etc/strongswan.d/charon/farp.conf",
        "mode"    => '0644',
        "owner"   => 'root',
        "group"   => 'root',
        "require" => "Package[strongswan]",
        "notify"  => "Class[Strongswan::Service]",
      )
    }

  end
end
